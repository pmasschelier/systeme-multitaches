#import "sourcerer.typ": code

#set quote(block: true)
#show quote: set pad(x: 2em)
#show quote: set text(fill: rgb(50, 50, 50), style: "italic")

= Travaux Dirigés - Systèmes multitâches

= Préambule

On déclare respectivement un thread un sémaphore et un mutex.
#code[```c
pthread_t thread_1; // Nom du thread
sem_t *semaphore; // Nom du sémaphore

// Nom du mutex
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
```]

Ensuite dans le main on crée le thread avec la fonction pthread_create
#code[```c
pthread_create(&thread_1, NULL, produce, NULL);
```]

Le point d'entrée de ce thread est l'adresse de la fonction produce. _thread_ va donc commencer à executer le code de cette fonction
#code[```c
// Point d'entrée du thread
void *produce(void *params)
{
    printf("Trying to own the mutex\n");
    pthread_mutex_lock(&mutex);
		...
```]

Puis dans le thread principal _main_ on attend la fin du thread _produce_ pour terminer le processus courant :
#code[```c
pthread_join(thread_1, NULL);
```]

On peut ensuite lancer notre programme à l'aide d'un shell.

La conception de ce programme n'est pas complète car elle n'intègre pas son comportement en cas d'erreur.

#image(width: 100%, "exercice-1/diagrams/preambule.svg")

= Premier exercice (Exercice Principal)

- *Exigence 1 :* MultitaskingAccumulator doit acquérir quatre entrées et produire en sortie le cumul des entrées dès qu’une entrée est acquise. Le cumul correspond à la production d’un tableau de 256 entiers dont le ieme élément de ce tableau est la somme des iemes éléments des tableaux d’entrée.

- *Exigence 2 :* MultitaskingAccumulator doit acquérir toutes les données d’entrées.

- *Exigence 3 :* MultitaskingAccumulator doit garantir que les données d’entrée sont correctement formées avant de les sommer.

- *Exigence 4 :* MultitaskingAccumulator doit sommer et produire une sortie au plus vite, c’est-à-dire dès qu’une entrée est présente sans attendre une nouvelle valeur sur chacune des entrées.

- *Exigence 5 :* Le logiciel doit produire sur la sortie diagnostic pour chaque opération de cumul, combien d’entrées ont été acquises, combien ont été sommées et combien restent à sommers

#image(width: 95%, "exercice-1/diagrams/conception.drawio.svg")

#quote[
	Une approche dirigée par les événements est-elle une approche synchrone ou asynchrone ?
]
Une approche dirigée par les événements est typiquement asynchrone (et peut par exemple reposer sur des interruptions).

#quote[
	A quelle stratégie de sûreté la méthode messageCheck répond-t-elle ?
]

La méthode messageCheck calcule le checksum d'un message pour vérifier que son contenu n'a pas été altéré. Cela correspond à une stratégie de *contrôle d'intégrité*.

#quote[
	A ce stade de l'implémentation squelette proposée, combien y a-t'il de processus (process) et de l d'exécution (thread) POSIX dans ce programme ?
]

Dans cette implémentation il y a un processus et 7 threads : le thread principal, le thread _display_, le thread _consumer_ et les quatre threads _producers_.


#image(width: 100%, "classes.svg")
#image(width: 100%, "sequence.svg")

#code[```sh
[multitaskingAccumulator]Software initialization in progress...
[acquisitionManager]Synchronization initialization in progress...
[acquisitionManager]Semaphore created
[acquisitionManager]Synchronization initialization done.
[multitaskingAccumulator]Task initialization done.
[multitaskingAccumulator]Scheduling in progress...
[OK      ] Checksum validated
Message
[displayManager]Produced messages: 3, Consumed messages: 1, Messages left: 2
[acquisitionManager] 60119 termination
[acquisitionManager] 60118 termination
[acquisitionManager] 60117 termination
[OK      ] Checksum validated
Message
[displayManager]Produced messages: 7, Consumed messages: 2, Messages left: 5
[displayManager] 60121 termination
[acquisitionManager] 60116 termination
[messageAdder] 60120 termination
[acquisitionManager]Semaphore cleaned
[multitaskingAccumulator]Threads terminated
```]

#quote[
	Pourquoi certaines variables sont-elles considérées comme des variables C volatiles dans l'implémentation proposée ?
]

Les variables _produceCount_, _consumeCount_ et _out_ notamment sont déclarées volatile car le compilateur doit être averti que leur valeur peut être modifiée par un autre thread que le thread courant.

#quote[
	Nous avons orienté l'achitecture détaillée avec l’utilisation des tâches. Nous aurions pu utiliser des processus POSIX. Citez les caractéristiques intéressantes des processus pour une conception orientée « sûreté de fonctionnement » ? Citez également celles qui ne sont pas satisfaites ?
	Auriez-vous pu utiliser les mêmes choix de conception que ceux-ci-dessus ?
]

Les processus POSIX ont un espace mémoire propre ce qui permet de partitionner spatialement le programme.
En les utilisant nous n'aurions donc pas pu utiliser de mémoire partagée entre les taches.

#quote[
	Proposez une solution pour protéger de manière efficace entre les tâches et sans utilisez des apis POSIX, le compteur permettant compter le nombre de messages produits ?
]
En déclarant le compteur en tant que *variable atomique*, celui ci peut être incrémenté de manière atomique par tous les producteurs sans utiliser d'objets de synchronisation POSIX.

#code[```bash
[multitaskingAccumulator]Software initialization in progress...
[acquisitionManager]Synchronization initialization in progress...
[acquisitionManager]Semaphore created
[acquisitionManager]Synchronization initialization done.
[multitaskingAccumulator]Task initialization done.
[multitaskingAccumulator]Scheduling in progress...
[OK      ] Checksum validated
Message
[displayManager]Produced messages: 3, Consumed messages: 1, Messages left: 2
[acquisitionManager] 61114 termination
[acquisitionManager] 61113 termination
[acquisitionManager] 61112 termination
[OK      ] Checksum validated
Message
[displayManager]Produced messages: 7, Consumed messages: 2, Messages left: 5
[displayManager] 61116 termination
[acquisitionManager] 61111 termination
[messageAdder] 61115 termination
[acquisitionManager]Semaphore cleaned
[multitaskingAccumulator]Threads terminated
```]

#quote[
	Une approche dirigée par le temps est-elle une approche synchrone ou asynchrone ?
]
Une approche dirigée par les événements est typiquement synchrone.

