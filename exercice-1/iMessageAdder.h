#ifndef I_MESSAGE_ADDER_H
#define I_MESSAGE_ADDER_H

#include "msg.h"

/**
* Gets a message that represents the current value of the sum.
*/
MSG_BLOCK getCurrentSum();

/**
* Get the number of consumed messages.
*/
unsigned int getConsumedCount();

/*
* Get atomically the current value of the sum and the number of consumed messages
*/
void getStatus(unsigned int* count, MSG_BLOCK* sum);

#endif