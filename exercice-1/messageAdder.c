#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h> 
#include <unistd.h>
#include <pthread.h>
#include "messageAdder.h"
#include "msg.h"
#include "iMessageAdder.h"
#include "multitaskingAccumulator.h"
#include "iAcquisitionManager.h"
#include "debug.h"

//consumer thread
pthread_t consumer;
//Message computed
volatile MSG_BLOCK out;
//Consumer count storage
volatile unsigned int consumeCount = 0;

pthread_mutex_t out_mutex, consume_count_mutex, status_mutex;

/**
 * Increments the consume count.
 */
static void incrementConsumeCount(void);

/**
 * Consumer entry point.
 */
static void *sum( void *parameters );


MSG_BLOCK getCurrentSum(){
	// DEBUT CODE
	MSG_BLOCK msg;
	pthread_mutex_lock(&out_mutex);
	msg = out;
	pthread_mutex_unlock(&out_mutex);
	return msg;
	// FIN CODE
}

unsigned int getConsumedCount(){
	// DEBUT CODE
	unsigned int count;
	pthread_mutex_lock(&consume_count_mutex);
	count = consumeCount;
	pthread_mutex_unlock(&consume_count_mutex);
	return count;
	// FIN CODE
}

void getStatus(unsigned int* count, MSG_BLOCK* sum) {
	pthread_mutex_lock(&status_mutex);
	*count = getConsumedCount();
	*sum = getCurrentSum();
	pthread_mutex_unlock(&status_mutex);
}


void messageAdderInit(void){
	out.checksum = 0;
	for (size_t i = 0; i < DATA_SIZE; i++)
	{
		out.mData[i] = 0;
	}
	// DEBUT CODE
	pthread_mutex_init(&status_mutex, NULL);
	pthread_mutex_init(&out_mutex, NULL);
	pthread_mutex_init(&consume_count_mutex, NULL);
	pthread_create(&consumer, NULL, sum, NULL);
	// FIN CODE
}

void messageAdderJoin(void){
	// DEBUT CODE
	pthread_join(consumer, NULL);
	// FIN CODE
}

static void incrementConsumeCount(void)
{
	// DEBUT CODE
	pthread_mutex_lock(&consume_count_mutex);
	consumeCount++;
	pthread_mutex_unlock(&consume_count_mutex);
	// FIN CODE
}

static void *sum( void *parameters )
{
	D(printf("[messageAdder]Thread created for sum with id %d\n", gettid()));
	unsigned int i = 0;

	while(i<ADDER_LOOP_LIMIT){
		i++;
		sleep(ADDER_SLEEP_TIME);

		// DEBUT CODE
		MSG_BLOCK msg;
		msg = getMessage();

		pthread_mutex_lock(&status_mutex);

		pthread_mutex_lock(&out_mutex);
		messageAdd(&out, &msg);
		pthread_mutex_unlock(&out_mutex);

		incrementConsumeCount();

		pthread_mutex_unlock(&status_mutex);
		// FIN CODE
	}
	printf("[messageAdder] %d termination\n", gettid());
	// DEBUT CODE
	return NULL;
	// FIN CODE
}


