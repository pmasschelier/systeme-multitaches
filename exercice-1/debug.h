//C macro to active debug printf
#ifdef DEBUG
#  define D(x) x
#else
#  define D(x) 
#endif

#include <unistd.h>
#include <sys/syscall.h>

#define gettid() ((pid_t)syscall(SYS_gettid))