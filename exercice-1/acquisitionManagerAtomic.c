#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include "acquisitionManager.h"
#include "msg.h"
#include "iSensor.h"
#include "multitaskingAccumulator.h"
#include "iAcquisitionManager.h"
#include "debug.h"


//producer count storage
_Atomic unsigned int produceCount;

pthread_t producers[PRODUCER_COUNT];

static void *produce(void *params);

// DEBUT CODE
#define BUFFER_SIZE 32

MSG_BLOCK messages[BUFFER_SIZE];
volatile int start = 0, end = 0;

static void push(volatile MSG_BLOCK const * msg) {
	messages[end] = *msg;
	end = (end + 1) % BUFFER_SIZE;
}

static MSG_BLOCK pull() {
	MSG_BLOCK msg = messages[start];
	start = (start + 1) % BUFFER_SIZE;
	return msg;
}

// FIN CODE

/**
* Semaphores and Mutex
*/
// DEBUT CODE
sem_t sem_libre, sem_occ;
pthread_mutex_t writer_mutex; // Mutex to protect the writer count
// FIN CODE

/*
* Creates the synchronization elements.
* @return ERROR_SUCCESS if the init is ok, ERROR_INIT otherwise
*/
static unsigned int createSynchronizationObjects(void);

/*
* Increments the produce count.
*/
static void incrementProducedCount(void);

static unsigned int createSynchronizationObjects(void)
{

	// DEBUT CODE
	if(0 != sem_init(&sem_libre, 0, BUFFER_SIZE))
		goto error3;
	if(0 != sem_init(&sem_occ, 0, 0))
		goto error2;
	if(0 != pthread_mutex_init(&writer_mutex, NULL))
		goto error1;
	// FIN CODE
	printf("[acquisitionManager]Semaphore created\n");
	return ERROR_SUCCESS;
error1:
	sem_close(&sem_libre);
error2:
	sem_close(&sem_occ);
error3:
	return ERROR_INIT;
}

static void incrementProducedCount(void)
{
	// DEBUT CODE
	produceCount++;
	// FIN CODE
}

unsigned int getProducedCount(void)
{
	unsigned int p = 0;
	// DEBUT CODE
	p = produceCount;
	// FIN CODE
	return p;
}

MSG_BLOCK getMessage(void){
	MSG_BLOCK msg;
	// DEBUT CODE
	sem_wait(&sem_occ);
	pthread_mutex_lock(&writer_mutex);

	msg = pull();

	pthread_mutex_unlock(&writer_mutex);
	sem_post(&sem_libre);
	return msg;
	// FIN CODE
}

//TODO create accessors to limit semaphore and mutex usage outside of this C module.

unsigned int acquisitionManagerInit(void)
{
	unsigned int i;
	printf("[acquisitionManager]Synchronization initialization in progress...\n");
	fflush( stdout );
	if (createSynchronizationObjects() == ERROR_INIT)
		return ERROR_INIT;
	
	printf("[acquisitionManager]Synchronization initialization done.\n");

	for (i = 0; i < PRODUCER_COUNT; i++)
	{
		// DEBUT CODE
		pthread_create(&producers[i], NULL, produce, (void*)i);
		// FIN CODE
	}

	return ERROR_SUCCESS;
}

void acquisitionManagerJoin(void)
{
	unsigned int i;
	for (i = 0; i < PRODUCER_COUNT; i++)
	{
		// DEBUT CODE
		pthread_join(producers[i], NULL);
		// FIN CODE
	}

	// DEBUT CODE
	sem_close(&sem_libre);
	sem_close(&sem_occ);
	// FIN CODE
	printf("[acquisitionManager]Semaphore cleaned\n");
}

void *produce(void* params)
{
	D(printf("[acquisitionManager]Producer created with id %d\n", gettid()));
	unsigned int i = 0;
	while (i < PRODUCER_LOOP_LIMIT)
	{
		i++;
		sleep(PRODUCER_SLEEP_TIME+(rand() % 5));
		// DEBUT CODE
		unsigned int input = (unsigned int)params;
		MSG_BLOCK msg;
		getInput(input, &msg);

		sem_wait(&sem_libre);
		pthread_mutex_lock(&writer_mutex);

		push(&msg);
		incrementProducedCount();

		pthread_mutex_unlock(&writer_mutex);
		sem_post(&sem_occ);
		// FIN CODE
	}
	printf("[acquisitionManager] %d termination\n", gettid());
	// DEBUT CODE
	return NULL;
	// FIN CODE
}