#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include "displayManager.h"
#include "iDisplay.h"
#include "iAcquisitionManager.h"
#include "iMessageAdder.h"
#include "msg.h"
#include "multitaskingAccumulator.h"
#include "debug.h"

// DisplayManager thread.
pthread_t displayThread;

/**
 * Display manager entry point.
 * */
static void *display(__attribute_maybe_unused__ void *parameters );


void displayManagerInit(void){
	// BEGIN CODE
	pthread_create(&displayThread, NULL, display, NULL);
	// END CODE
}

void displayManagerJoin(void){
	// BEGIN CODE
	pthread_join(displayThread, NULL);
	// END CODE
} 

static void *display(__attribute_maybe_unused__ void *parameters )
{
	D(printf("[displayManager]Thread created for display with id %d\n", gettid()));
	unsigned int diffCount = 0;
	while(diffCount < DISPLAY_LOOP_LIMIT){
		sleep(DISPLAY_SLEEP_TIME);
		// BEGIN CODE
		diffCount++;
		MSG_BLOCK msg;
		unsigned int consumedCount;
		getStatus(&consumedCount, &msg);
		messageDisplay(&msg);

		print(getProducedCount(), consumedCount);
		// END CODE
	}
	printf("[displayManager] %d termination\n", gettid());
	// BEGIN CODE
	return NULL;
	// END CODE
}